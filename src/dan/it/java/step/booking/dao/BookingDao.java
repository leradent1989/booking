package dan.it.java.step.booking.dao;

import dan.it.java.step.booking.domain.Booking;
import dan.it.java.step.booking.domain.Flight;
import dan.it.java.step.booking.domain.User;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class BookingDao {
    private List <Booking> bookingList;


    public BookingDao(List <Booking> bookingList) {

        this.bookingList = bookingList;

    }
    public void loadData (){
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("listBookings"))) {
            objectOutputStream.writeObject(bookingList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Booking> getBookingList(){
        List <Booking> bookings;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("listBookings") ) ) {
           bookings = (List<Booking>) objectInputStream.readObject();

        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return bookings;
    }
    public Booking addBooking(List <User> passengers, Flight flight){


        int passengerNumber = passengers.size();
      Booking booking =  new Booking(passengers,passengerNumber,flight);

        bookingList.add(booking);

                passengers.forEach(el->el.getBooking().add(booking));

        flight.setFreeSeats(flight.getFreeSeats() -1);

        passengers.forEach(el-> flight.getPassengers().add(el));
        return  booking;
    }

    public void deleteBooking (int bookingId ){



        Booking booking =  bookingList.stream()

                .filter(el -> el.getId() == bookingId)

                .collect(Collectors.toList()).get(0);

        bookingList.remove(booking);

        booking.getPassengerList().forEach(el -> el.getBooking().remove(booking));

        Flight flight = booking.getFlight();

        flight.setFreeSeats(flight.getFreeSeats() -1);

        booking.getPassengerList().forEach(el-> {

            flight.getPassengers().remove(el);

            el.getBooking().remove(booking);});

    }


    public List<Booking>  findAll() {
        return bookingList;

    }




}
